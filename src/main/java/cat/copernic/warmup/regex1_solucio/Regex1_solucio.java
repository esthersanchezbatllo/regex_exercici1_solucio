/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package cat.copernic.warmup.regex1_solucio;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


public class Regex1_solucio {

    private static Scanner sc = new Scanner(System.in);

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
    
        
        //Exercici 1
        //En les matrícules españoles estan prohibides nou lletres: A, CH, E, I, LL, Ñ, O, Q y U
        final String regexNewPlate ="\\d{4}[ ][BCDFGHJKLMNPRSTVWXYZ]{3}";
         //No cal posar una àncora al principi i al final si utilitzem el mètode matches(). 
        //Si utilitzessim find() si que hauriem de posar les àncores.
        
        System.out.println("Exercici 1\n");
              
         try {
            
            // Pattern verifica lexpressió regular
            // No es pot crear l'objecte si l'expressió és incorrecta a nivell sintàctic
            Pattern regles1 = Pattern.compile(regexNewPlate); 
            //Important posar-ho a dins del try perquè si no ho està donaria una excepció i petaria el programa.
            
            System.out.print("Introdueix una matrícula nova d'Espanya (4 digits, 1 espai, 3 consonants en majúscules): ");
            String entrada1 = sc.nextLine();
            Matcher resultat1 = regles1.matcher(entrada1);

            // matches comprova que la seqüencia SENCERA coincideixi amb l'expressió regular, en canvi find busca que CONTINGUI una o més coincidencies
            if (resultat1.matches()) {
                System.out.println("Matrí­cula nova vàlida ");
            } else {
                System.out.println("Matrí­cula nova invàlida");
            }

        } catch (PatternSyntaxException e) {
            System.out.println("Error de sintaxis­ en el patró");
        }

        System.out.println("");

        //Exercici 2
        System.out.println("Exercici 2\n");

      //Cadena a analitzar
       String defText = "Ha estat molt poc Temps el que hi has dedicat....";
       
       // Expressió regular que fa match si:
       //    conté entre 3 i 6 caràcters de la "a" a la "z"
       //    ,seguida d'un caràcter que és una 't' o una 's'
       String expresioRegularEx2 = "[a-z]{3,6}[ts]";

      try{
            Pattern regles = Pattern.compile(expresioRegularEx2);

            //Matcher busca l'expressió regular a la cadena i emmagatzema les N coincidencies
            Matcher matcher = regles.matcher(defText);

            //recorrem matcher per comptar les coincidencies
            System.out.println(String.format("S'han trobat %d coincidencies: ", matcher.results().count()));
          
            //tornem enrere el matcher (reset) per anar una per una des del principi
            matcher.reset();

             //Es repeteix tants cops com coincidencies (match) es trobin:
             while (matcher.find()){

                 System.out.println();
                 //mostrem la cadena coincident
                 System.out.println("Coincidència:" + matcher.group() );
                 //mostrem el índex inicial i final de la cadena coincident
                 System.out.println("Start:" + matcher.start() );
                 System.out.println("End:" + matcher.end() );
                
                 System.out.println();

                 // Més info de la classe Matcher a JAVADOC:
                 // https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/regex/Matcher.html
             }
       } catch (PatternSyntaxException e) {
            System.out.println("Error de sintaxis­ en el patró");
        }
    }
    
   

    
}
